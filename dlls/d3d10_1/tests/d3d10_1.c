/*
 * Copyright 2008 Henri Verbeet for CodeWeavers
 * Copyright 2015 Józef Kucia for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#define COBJMACROS
#include "d3d10_1.h"
#include "initguid.h"
#include "d3d11_1.h"
#include "wine/test.h"

static const D3D10_FEATURE_LEVEL1 d3d10_feature_levels[] =
{
    D3D10_FEATURE_LEVEL_10_1,
    D3D10_FEATURE_LEVEL_10_0,
    D3D10_FEATURE_LEVEL_9_3,
    D3D10_FEATURE_LEVEL_9_2,
    D3D10_FEATURE_LEVEL_9_1
};

static ULONG get_refcount(IUnknown *iface)
{
    IUnknown_AddRef(iface);
    return IUnknown_Release(iface);
}

struct device_desc
{
    D3D10_FEATURE_LEVEL1 feature_level;
    UINT flags;
};

static ID3D10Device1 *create_device(const struct device_desc *desc)
{
    D3D10_FEATURE_LEVEL1 feature_level = D3D10_FEATURE_LEVEL_10_1;
    ID3D10Device1 *device;
    UINT flags = 0;

    if (desc)
    {
        feature_level = desc->feature_level;
        flags = desc->flags;
    }

    if (SUCCEEDED(D3D10CreateDevice1(NULL, D3D10_DRIVER_TYPE_HARDWARE,
            NULL, flags, feature_level, D3D10_1_SDK_VERSION, &device)))
        return device;
    if (SUCCEEDED(D3D10CreateDevice1(NULL, D3D10_DRIVER_TYPE_WARP,
            NULL, flags, feature_level, D3D10_1_SDK_VERSION, &device)))
        return device;
    if (SUCCEEDED(D3D10CreateDevice1(NULL, D3D10_DRIVER_TYPE_REFERENCE,
            NULL, flags, feature_level, D3D10_1_SDK_VERSION, &device)))
        return device;

    return NULL;
}

#define check_interface(a, b, c, d) check_interface_(__LINE__, a, b, c, d)
static HRESULT check_interface_(unsigned int line, void *iface, REFIID iid, BOOL supported, BOOL is_broken)
{
    HRESULT hr, expected_hr, broken_hr;
    IUnknown *unknown = iface, *out;

    if (supported)
    {
        expected_hr = S_OK;
        broken_hr = E_NOINTERFACE;
    }
    else
    {
        expected_hr = E_NOINTERFACE;
        broken_hr = S_OK;
    }

    hr = IUnknown_QueryInterface(unknown, iid, (void **)&out);
    ok_(__FILE__, line)(hr == expected_hr || broken(is_broken && hr == broken_hr),
            "Got unexpected hr %#lx, expected %#lx.\n", hr, expected_hr);
    if (SUCCEEDED(hr))
        IUnknown_Release(out);
    return hr;
}

static void test_create_device(void)
{
    D3D10_FEATURE_LEVEL1 feature_level, supported_feature_level;
    DXGI_SWAP_CHAIN_DESC swapchain_desc, obtained_desc;
    IDXGISwapChain *swapchain;
    ID3D10Device1 *device;
    unsigned int i;
    ULONG refcount;
    HWND window;
    HRESULT hr;

    for (i = 0; i < ARRAY_SIZE(d3d10_feature_levels); ++i)
    {
        if (SUCCEEDED(hr = D3D10CreateDevice1(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0,
                d3d10_feature_levels[i], D3D10_1_SDK_VERSION, &device)))
        {
            supported_feature_level = d3d10_feature_levels[i];
            break;
        }
    }

    if (FAILED(hr))
    {
        skip("Failed to create HAL device.\n");
        return;
    }

    feature_level = ID3D10Device1_GetFeatureLevel(device);
    ok(feature_level == supported_feature_level, "Got feature level %#x, expected %#x.\n",
            feature_level, supported_feature_level);

    ID3D10Device1_Release(device);

    hr = D3D10CreateDevice1(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0,
            supported_feature_level, D3D10_1_SDK_VERSION, NULL);
    ok(hr == E_INVALIDARG, "Got unexpected hr %#lx.\n", hr);

    device = (ID3D10Device1 *)0xdeadbeef;
    hr = D3D10CreateDevice1(NULL, 0xffffffff, NULL, 0,
            supported_feature_level, D3D10_1_SDK_VERSION, &device);
    todo_wine ok(hr == E_INVALIDARG, "Got unexpected hr %#lx.\n", hr);
    ok(!device, "Got unexpected device pointer %p.\n", device);

    device = (ID3D10Device1 *)0xdeadbeef;
    hr = D3D10CreateDevice1(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0,
            0, D3D10_1_SDK_VERSION, &device);
    ok(hr == E_INVALIDARG, "Got unexpected hr %#lx.\n", hr);
    ok(!device, "Got unexpected device pointer %p.\n", device);

    window = CreateWindowA("static", "d3d10_1_test", 0, 0, 0, 0, 0, 0, 0, 0, 0);

    swapchain_desc.BufferDesc.Width = 800;
    swapchain_desc.BufferDesc.Height = 600;
    swapchain_desc.BufferDesc.RefreshRate.Numerator = 60;
    swapchain_desc.BufferDesc.RefreshRate.Denominator = 60;
    swapchain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapchain_desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swapchain_desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    swapchain_desc.SampleDesc.Count = 1;
    swapchain_desc.SampleDesc.Quality = 0;
    swapchain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapchain_desc.BufferCount = 1;
    swapchain_desc.OutputWindow = window;
    swapchain_desc.Windowed = TRUE;
    swapchain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
    swapchain_desc.Flags = 0;

    hr = D3D10CreateDeviceAndSwapChain1(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0,
            supported_feature_level, D3D10_1_SDK_VERSION, &swapchain_desc, &swapchain, &device);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);

    check_interface(swapchain, &IID_IDXGISwapChain1, TRUE, FALSE);

    memset(&obtained_desc, 0, sizeof(obtained_desc));
    hr = IDXGISwapChain_GetDesc(swapchain, &obtained_desc);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);
    ok(obtained_desc.BufferDesc.Width == swapchain_desc.BufferDesc.Width,
            "Got unexpected BufferDesc.Width %u.\n", obtained_desc.BufferDesc.Width);
    ok(obtained_desc.BufferDesc.Height == swapchain_desc.BufferDesc.Height,
            "Got unexpected BufferDesc.Height %u.\n", obtained_desc.BufferDesc.Height);
    todo_wine ok(obtained_desc.BufferDesc.RefreshRate.Numerator == swapchain_desc.BufferDesc.RefreshRate.Numerator,
            "Got unexpected BufferDesc.RefreshRate.Numerator %u.\n",
            obtained_desc.BufferDesc.RefreshRate.Numerator);
    todo_wine ok(obtained_desc.BufferDesc.RefreshRate.Denominator == swapchain_desc.BufferDesc.RefreshRate.Denominator,
            "Got unexpected BufferDesc.RefreshRate.Denominator %u.\n",
            obtained_desc.BufferDesc.RefreshRate.Denominator);
    ok(obtained_desc.BufferDesc.Format == swapchain_desc.BufferDesc.Format,
            "Got unexpected BufferDesc.Format %#x.\n", obtained_desc.BufferDesc.Format);
    ok(obtained_desc.BufferDesc.ScanlineOrdering == swapchain_desc.BufferDesc.ScanlineOrdering,
            "Got unexpected BufferDesc.ScanlineOrdering %#x.\n", obtained_desc.BufferDesc.ScanlineOrdering);
    ok(obtained_desc.BufferDesc.Scaling == swapchain_desc.BufferDesc.Scaling,
            "Got unexpected BufferDesc.Scaling %#x.\n", obtained_desc.BufferDesc.Scaling);
    ok(obtained_desc.SampleDesc.Count == swapchain_desc.SampleDesc.Count,
            "Got unexpected SampleDesc.Count %u.\n", obtained_desc.SampleDesc.Count);
    ok(obtained_desc.SampleDesc.Quality == swapchain_desc.SampleDesc.Quality,
            "Got unexpected SampleDesc.Quality %u.\n", obtained_desc.SampleDesc.Quality);
    ok(obtained_desc.BufferUsage == swapchain_desc.BufferUsage,
            "Got unexpected BufferUsage %#x.\n", obtained_desc.BufferUsage);
    ok(obtained_desc.BufferCount == swapchain_desc.BufferCount,
            "Got unexpected BufferCount %u.\n", obtained_desc.BufferCount);
    ok(obtained_desc.OutputWindow == swapchain_desc.OutputWindow,
            "Got unexpected OutputWindow %p.\n", obtained_desc.OutputWindow);
    ok(obtained_desc.Windowed == swapchain_desc.Windowed,
            "Got unexpected Windowed %#x.\n", obtained_desc.Windowed);
    ok(obtained_desc.SwapEffect == swapchain_desc.SwapEffect,
            "Got unexpected SwapEffect %#x.\n", obtained_desc.SwapEffect);
    ok(obtained_desc.Flags == swapchain_desc.Flags,
            "Got unexpected Flags %#x.\n", obtained_desc.Flags);

    refcount = IDXGISwapChain_Release(swapchain);
    ok(!refcount, "Swapchain has %lu references left.\n", refcount);

    feature_level = ID3D10Device1_GetFeatureLevel(device);
    ok(feature_level == supported_feature_level, "Got feature level %#x, expected %#x.\n",
            feature_level, supported_feature_level);

    refcount = ID3D10Device1_Release(device);
    ok(!refcount, "Device has %lu references left.\n", refcount);

    hr = D3D10CreateDeviceAndSwapChain1(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0,
            supported_feature_level, D3D10_1_SDK_VERSION, NULL, NULL, &device);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);
    refcount = ID3D10Device1_Release(device);
    ok(!refcount, "Device has %lu references left.\n", refcount);

    hr = D3D10CreateDeviceAndSwapChain1(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0,
            supported_feature_level, D3D10_1_SDK_VERSION, NULL, NULL, NULL);
    ok(hr == E_INVALIDARG, "Got unexpected hr %#lx.\n", hr);

    hr = D3D10CreateDeviceAndSwapChain1(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0,
            supported_feature_level, D3D10_1_SDK_VERSION, &swapchain_desc, NULL, NULL);
    ok(hr == E_INVALIDARG, "Got unexpected hr %#lx.\n", hr);

    swapchain = (IDXGISwapChain *)0xdeadbeef;
    device = (ID3D10Device1 *)0xdeadbeef;
    hr = D3D10CreateDeviceAndSwapChain1(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0,
            0, D3D10_1_SDK_VERSION, &swapchain_desc, &swapchain, &device);
    ok(hr == E_INVALIDARG, "Got unexpected hr %#lx.\n", hr);
    ok(!swapchain, "Got unexpected swapchain pointer %p.\n", swapchain);
    ok(!device, "Got unexpected device pointer %p.\n", device);

    swapchain = (IDXGISwapChain *)0xdeadbeef;
    hr = D3D10CreateDeviceAndSwapChain1(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0,
            supported_feature_level, D3D10_1_SDK_VERSION, &swapchain_desc, &swapchain, NULL);
    ok(hr == E_INVALIDARG, "Got unexpected hr %#lx.\n", hr);
    ok(!swapchain, "Got unexpected swapchain pointer %p.\n", swapchain);

    swapchain_desc.OutputWindow = NULL;
    hr = D3D10CreateDeviceAndSwapChain1(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0,
            supported_feature_level, D3D10_1_SDK_VERSION, &swapchain_desc, NULL, &device);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);
    refcount = ID3D10Device1_Release(device);
    ok(!refcount, "Device has %lu references left.\n", refcount);

    swapchain = (IDXGISwapChain *)0xdeadbeef;
    device = (ID3D10Device1 *)0xdeadbeef;
    swapchain_desc.OutputWindow = NULL;
    hr = D3D10CreateDeviceAndSwapChain1(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0,
            supported_feature_level, D3D10_1_SDK_VERSION, &swapchain_desc, &swapchain, &device);
    ok(hr == DXGI_ERROR_INVALID_CALL, "Got unexpected hr %#lx.\n", hr);
    ok(!swapchain, "Got unexpected swapchain pointer %p.\n", swapchain);
    ok(!device, "Got unexpected device pointer %p.\n", device);

    swapchain = (IDXGISwapChain *)0xdeadbeef;
    device = (ID3D10Device1 *)0xdeadbeef;
    swapchain_desc.OutputWindow = window;
    swapchain_desc.BufferDesc.Format = DXGI_FORMAT_BC5_UNORM;
    hr = D3D10CreateDeviceAndSwapChain1(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, 0,
            supported_feature_level, D3D10_1_SDK_VERSION, &swapchain_desc, &swapchain, &device);
    ok(hr == E_INVALIDARG, "Got unexpected hr %#lx.\n", hr);
    ok(!swapchain, "Got unexpected swapchain pointer %p.\n", swapchain);
    ok(!device, "Got unexpected device pointer %p.\n", device);

    DestroyWindow(window);
}

static void test_device_interfaces(void)
{
    IDXGIAdapter *dxgi_adapter;
    IDXGIDevice *dxgi_device;
    ID3D10Device1 *device;
    IUnknown *iface;
    ULONG refcount;
    unsigned int i;
    HRESULT hr;

    for (i = 0; i < ARRAY_SIZE(d3d10_feature_levels); ++i)
    {
        struct device_desc device_desc;

        device_desc.feature_level = d3d10_feature_levels[i];
        device_desc.flags = 0;

        if (!(device = create_device(&device_desc)))
        {
            skip("Failed to create device for feature level %#x.\n", d3d10_feature_levels[i]);
            continue;
        }

        check_interface(device, &IID_IUnknown, TRUE, FALSE);
        check_interface(device, &IID_IDXGIObject, TRUE, FALSE);
        check_interface(device, &IID_IDXGIDevice, TRUE, FALSE);
        check_interface(device, &IID_IDXGIDevice1, TRUE, FALSE);
        check_interface(device, &IID_ID3D10Multithread, TRUE, TRUE); /* Not available on all Windows versions. */
        check_interface(device, &IID_ID3D10Device, TRUE, FALSE);
        check_interface(device, &IID_ID3D10InfoQueue, FALSE, FALSE); /* Non-debug mode. */
        check_interface(device, &IID_ID3D11Device, TRUE, TRUE); /* Not available on all Windows versions. */

        hr = ID3D10Device1_QueryInterface(device, &IID_IDXGIDevice, (void **)&dxgi_device);
        ok(SUCCEEDED(hr), "Device should implement IDXGIDevice.\n");
        hr = IDXGIDevice_GetParent(dxgi_device, &IID_IDXGIAdapter, (void **)&dxgi_adapter);
        ok(SUCCEEDED(hr), "Device parent should implement IDXGIAdapter.\n");
        hr = IDXGIAdapter_GetParent(dxgi_adapter, &IID_IDXGIFactory, (void **)&iface);
        ok(SUCCEEDED(hr), "Adapter parent should implement IDXGIFactory.\n");
        IUnknown_Release(iface);
        IDXGIAdapter_Release(dxgi_adapter);
        hr = IDXGIDevice_GetParent(dxgi_device, &IID_IDXGIAdapter1, (void **)&dxgi_adapter);
        ok(SUCCEEDED(hr), "Device parent should implement IDXGIAdapter1.\n");
        hr = IDXGIAdapter_GetParent(dxgi_adapter, &IID_IDXGIFactory1, (void **)&iface);
        ok(hr == E_NOINTERFACE, "Adapter parent should not implement IDXGIFactory1.\n");
        IDXGIAdapter_Release(dxgi_adapter);
        IDXGIDevice_Release(dxgi_device);

        refcount = ID3D10Device1_Release(device);
        ok(!refcount, "Device has %lu references left.\n", refcount);
    }

    for (i = 0; i < ARRAY_SIZE(d3d10_feature_levels); ++i)
    {
        struct device_desc device_desc;

        device_desc.feature_level = d3d10_feature_levels[i];
        device_desc.flags = D3D10_CREATE_DEVICE_DEBUG;
        if (!(device = create_device(&device_desc)))
        {
            skip("Failed to create device for feature level %#x.\n", d3d10_feature_levels[i]);
            continue;
        }

        todo_wine
        check_interface(device, &IID_ID3D10InfoQueue, TRUE, FALSE);

        refcount = ID3D10Device1_Release(device);
        ok(!refcount, "Device has %lu references left.\n", refcount);
    }
}

static void test_create_shader_resource_view(void)
{
    D3D10_SHADER_RESOURCE_VIEW_DESC1 srv_desc;
    D3D10_TEXTURE2D_DESC texture_desc;
    ULONG refcount, expected_refcount;
    ID3D10ShaderResourceView1 *srview;
    D3D10_BUFFER_DESC buffer_desc;
    ID3D10Texture2D *texture;
    ID3D10Device *tmp_device;
    ID3D10Device1 *device;
    ID3D10Buffer *buffer;
    HRESULT hr;

    if (!(device = create_device(NULL)))
    {
        skip("Failed to create device.\n");
        return;
    }

    buffer_desc.ByteWidth = 1024;
    buffer_desc.Usage = D3D10_USAGE_DEFAULT;
    buffer_desc.BindFlags = D3D10_BIND_SHADER_RESOURCE;
    buffer_desc.CPUAccessFlags = 0;
    buffer_desc.MiscFlags = 0;

    hr = ID3D10Device1_CreateBuffer(device, &buffer_desc, NULL, &buffer);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);

    hr = ID3D10Device1_CreateShaderResourceView1(device, (ID3D10Resource *)buffer, NULL, &srview);
    ok(hr == E_INVALIDARG, "Got unexpected hr %#lx.\n", hr);

    srv_desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    srv_desc.ViewDimension = D3D10_1_SRV_DIMENSION_BUFFER;
    U(srv_desc).Buffer.ElementOffset = 0;
    U(srv_desc).Buffer.ElementWidth = 64;

    hr = ID3D10Device1_CreateShaderResourceView1(device, NULL, &srv_desc, &srview);
    ok(hr == E_INVALIDARG, "Got unexpected hr %#lx.\n", hr);

    expected_refcount = get_refcount((IUnknown *)device) + 1;
    hr = ID3D10Device1_CreateShaderResourceView1(device, (ID3D10Resource *)buffer, &srv_desc, &srview);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);
    refcount = get_refcount((IUnknown *)device);
    ok(refcount >= expected_refcount, "Got unexpected refcount %lu, expected >= %lu.\n", refcount, expected_refcount);
    tmp_device = NULL;
    expected_refcount = refcount + 1;
    ID3D10ShaderResourceView1_GetDevice(srview, &tmp_device);
    ok(tmp_device == (ID3D10Device *)device, "Got unexpected device %p, expected %p.\n", tmp_device, device);
    refcount = get_refcount((IUnknown *)device);
    ok(refcount == expected_refcount, "Got unexpected refcount %lu, expected %lu.\n", refcount, expected_refcount);
    ID3D10Device_Release(tmp_device);

    check_interface(srview, &IID_ID3D10ShaderResourceView, TRUE, FALSE);
    /* Not available on all Windows versions. */
    check_interface(srview, &IID_ID3D11ShaderResourceView, TRUE, TRUE);

    ID3D10ShaderResourceView1_Release(srview);
    ID3D10Buffer_Release(buffer);

    /* Without D3D10_BIND_SHADER_RESOURCE. */
    buffer_desc.ByteWidth = 1024;
    buffer_desc.Usage = D3D10_USAGE_DEFAULT;
    buffer_desc.BindFlags = 0;
    buffer_desc.CPUAccessFlags = 0;
    buffer_desc.MiscFlags = 0;

    hr = ID3D10Device1_CreateBuffer(device, &buffer_desc, NULL, &buffer);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);

    hr = ID3D10Device1_CreateShaderResourceView1(device, (ID3D10Resource *)buffer, &srv_desc, &srview);
    ok(hr == E_INVALIDARG, "Got unexpected hr %#lx.\n", hr);

    ID3D10Buffer_Release(buffer);

    texture_desc.Width = 512;
    texture_desc.Height = 512;
    texture_desc.MipLevels = 0;
    texture_desc.ArraySize = 1;
    texture_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    texture_desc.SampleDesc.Count = 1;
    texture_desc.SampleDesc.Quality = 0;
    texture_desc.Usage = D3D10_USAGE_DEFAULT;
    texture_desc.BindFlags = D3D10_BIND_SHADER_RESOURCE;
    texture_desc.CPUAccessFlags = 0;
    texture_desc.MiscFlags = 0;

    hr = ID3D10Device1_CreateTexture2D(device, &texture_desc, NULL, &texture);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);

    hr = ID3D10Device1_CreateShaderResourceView1(device, (ID3D10Resource *)texture, NULL, &srview);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);

    ID3D10ShaderResourceView1_GetDesc1(srview, &srv_desc);
    ok(srv_desc.Format == texture_desc.Format, "Got unexpected format %#x.\n", srv_desc.Format);
    ok(srv_desc.ViewDimension == D3D10_1_SRV_DIMENSION_TEXTURE2D,
            "Got unexpected view dimension %#x.\n", srv_desc.ViewDimension);
    ok(U(srv_desc).Texture2D.MostDetailedMip == 0, "Got unexpected MostDetailedMip %u.\n",
            U(srv_desc).Texture2D.MostDetailedMip);
    ok(U(srv_desc).Texture2D.MipLevels == 10, "Got unexpected MipLevels %u.\n", U(srv_desc).Texture2D.MipLevels);

    check_interface(srview, &IID_ID3D10ShaderResourceView, TRUE, FALSE);
    /* Not available on all Windows versions. */
    check_interface(srview, &IID_ID3D11ShaderResourceView, TRUE, TRUE);

    ID3D10ShaderResourceView1_Release(srview);
    ID3D10Texture2D_Release(texture);

    refcount = ID3D10Device1_Release(device);
    ok(!refcount, "Device has %lu references left.\n", refcount);
}

static void test_create_blend_state(void)
{
    static const D3D10_BLEND_DESC1 desc_conversion_tests[] =
    {
        {
            FALSE, FALSE,
            {
                {
                    FALSE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD
                },
            },
        },
        {
            FALSE, TRUE,
            {
                {
                    TRUE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
                {
                    FALSE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_RED
                },
                {
                    TRUE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
                {
                    FALSE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_GREEN
                },
                {
                    TRUE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
                {
                    TRUE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
                {
                    TRUE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
                {
                    TRUE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
            },
        },
        {
            FALSE, TRUE,
            {
                {
                    TRUE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
                {
                    TRUE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_SUBTRACT,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
                {
                    TRUE, D3D10_BLEND_ZERO, D3D10_BLEND_ONE, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
                {
                    TRUE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ZERO, D3D10_BLEND_ONE, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
                {
                    TRUE, D3D10_BLEND_ONE, D3D10_BLEND_ONE, D3D10_BLEND_OP_MAX,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
                {
                    TRUE, D3D10_BLEND_ONE, D3D10_BLEND_ONE, D3D10_BLEND_OP_MIN,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
                {
                    FALSE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
                {
                    FALSE, D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD,
                    D3D10_BLEND_ONE, D3D10_BLEND_ZERO, D3D10_BLEND_OP_ADD, D3D10_COLOR_WRITE_ENABLE_ALL
                },
            },
        },
    };

    ID3D10BlendState1 *blend_state1, *blend_state2;
    D3D10_BLEND_DESC1 desc, obtained_desc;
    ID3D10BlendState *d3d10_blend_state;
    D3D10_BLEND_DESC d3d10_blend_desc;
    ULONG refcount, expected_refcount;
    ID3D10Device1 *device;
    ID3D10Device *tmp;
    unsigned int i, j;
    HRESULT hr;

    if (!(device = create_device(NULL)))
    {
        skip("Failed to create device.\n");
        return;
    }

    hr = ID3D10Device1_CreateBlendState1(device, NULL, &blend_state1);
    ok(hr == E_INVALIDARG, "Got unexpected hr %#lx.\n", hr);

    memset(&desc, 0, sizeof(desc));
    desc.AlphaToCoverageEnable = FALSE;
    desc.IndependentBlendEnable = FALSE;
    desc.RenderTarget[0].BlendEnable = FALSE;
    desc.RenderTarget[0].SrcBlend = D3D10_BLEND_ONE;
    desc.RenderTarget[0].DestBlend = D3D10_BLEND_ZERO;
    desc.RenderTarget[0].BlendOp = D3D10_BLEND_OP_ADD;
    desc.RenderTarget[0].SrcBlendAlpha = D3D10_BLEND_ONE;
    desc.RenderTarget[0].DestBlendAlpha = D3D10_BLEND_ZERO;
    desc.RenderTarget[0].BlendOpAlpha = D3D10_BLEND_OP_ADD;
    desc.RenderTarget[0].RenderTargetWriteMask = D3D10_COLOR_WRITE_ENABLE_ALL;

    expected_refcount = get_refcount((IUnknown *)device) + 1;
    hr = ID3D10Device1_CreateBlendState1(device, &desc, &blend_state1);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);
    hr = ID3D10Device1_CreateBlendState1(device, &desc, &blend_state2);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);
    ok(blend_state1 == blend_state2, "Got different blend state objects.\n");
    refcount = get_refcount((IUnknown *)device);
    ok(refcount >= expected_refcount, "Got unexpected refcount %lu, expected >= %lu.\n", refcount, expected_refcount);
    tmp = NULL;
    expected_refcount = refcount + 1;
    ID3D10BlendState1_GetDevice(blend_state1, &tmp);
    ok(tmp == (ID3D10Device *)device, "Got unexpected device %p, expected %p.\n", tmp, device);
    refcount = get_refcount((IUnknown *)device);
    ok(refcount == expected_refcount, "Got unexpected refcount %lu, expected %lu.\n", refcount, expected_refcount);
    ID3D10Device_Release(tmp);

    ID3D10BlendState1_GetDesc1(blend_state1, &obtained_desc);
    ok(obtained_desc.AlphaToCoverageEnable == FALSE, "Got unexpected alpha to coverage enable %#x.\n",
            obtained_desc.AlphaToCoverageEnable);
    ok(obtained_desc.IndependentBlendEnable == FALSE, "Got unexpected independent blend enable %#x.\n",
            obtained_desc.IndependentBlendEnable);
    for (i = 0; i < D3D10_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i)
    {
        ok(obtained_desc.RenderTarget[i].BlendEnable == FALSE,
                "Got unexpected blend enable %#x for render target %u.\n",
                obtained_desc.RenderTarget[i].BlendEnable, i);
        ok(obtained_desc.RenderTarget[i].SrcBlend == D3D10_BLEND_ONE,
                "Got unexpected src blend %u for render target %u.\n",
                obtained_desc.RenderTarget[i].SrcBlend, i);
        ok(obtained_desc.RenderTarget[i].DestBlend == D3D10_BLEND_ZERO,
                "Got unexpected dest blend %u for render target %u.\n",
                obtained_desc.RenderTarget[i].DestBlend, i);
        ok(obtained_desc.RenderTarget[i].BlendOp == D3D10_BLEND_OP_ADD,
                "Got unexpected blend op %u for render target %u.\n",
                obtained_desc.RenderTarget[i].BlendOp, i);
        ok(obtained_desc.RenderTarget[i].SrcBlendAlpha == D3D10_BLEND_ONE,
                "Got unexpected src blend alpha %u for render target %u.\n",
                obtained_desc.RenderTarget[i].SrcBlendAlpha, i);
        ok(obtained_desc.RenderTarget[i].DestBlendAlpha == D3D10_BLEND_ZERO,
                "Got unexpected dest blend alpha %u for render target %u.\n",
                obtained_desc.RenderTarget[i].DestBlendAlpha, i);
        ok(obtained_desc.RenderTarget[i].BlendOpAlpha == D3D10_BLEND_OP_ADD,
                "Got unexpected blend op alpha %u for render target %u.\n",
                obtained_desc.RenderTarget[i].BlendOpAlpha, i);
        ok(obtained_desc.RenderTarget[i].RenderTargetWriteMask == D3D10_COLOR_WRITE_ENABLE_ALL,
                "Got unexpected render target write mask %#x for render target %u.\n",
                obtained_desc.RenderTarget[0].RenderTargetWriteMask, i);
    }

    check_interface(blend_state1, &IID_ID3D10BlendState, TRUE, FALSE);
    /* Not available on all Windows versions. */
    check_interface(blend_state1, &IID_ID3D11BlendState, TRUE, TRUE);

    refcount = ID3D10BlendState1_Release(blend_state1);
    ok(refcount == 1, "Got unexpected refcount %lu.\n", refcount);
    refcount = ID3D10BlendState1_Release(blend_state2);
    ok(!refcount, "Blend state has %lu references left.\n", refcount);

    for (i = 0; i < ARRAY_SIZE(desc_conversion_tests); ++i)
    {
        const D3D10_BLEND_DESC1 *current_desc = &desc_conversion_tests[i];

        hr = ID3D10Device1_CreateBlendState1(device, current_desc, &blend_state1);
        ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);

        hr = ID3D10BlendState1_QueryInterface(blend_state1, &IID_ID3D10BlendState, (void **)&d3d10_blend_state);
        ok(SUCCEEDED(hr), "Blend state should implement ID3D10BlendState.\n");

        ID3D10BlendState_GetDesc(d3d10_blend_state, &d3d10_blend_desc);
        ok(d3d10_blend_desc.AlphaToCoverageEnable == current_desc->AlphaToCoverageEnable,
                "Got unexpected alpha to coverage enable %#x for test %u.\n",
                d3d10_blend_desc.AlphaToCoverageEnable, i);
        ok(d3d10_blend_desc.SrcBlend == current_desc->RenderTarget[0].SrcBlend,
                "Got unexpected src blend %u for test %u.\n", d3d10_blend_desc.SrcBlend, i);
        ok(d3d10_blend_desc.DestBlend == current_desc->RenderTarget[0].DestBlend,
                "Got unexpected dest blend %u for test %u.\n", d3d10_blend_desc.DestBlend, i);
        ok(d3d10_blend_desc.BlendOp == current_desc->RenderTarget[0].BlendOp,
                "Got unexpected blend op %u for test %u.\n", d3d10_blend_desc.BlendOp, i);
        ok(d3d10_blend_desc.SrcBlendAlpha == current_desc->RenderTarget[0].SrcBlendAlpha,
                "Got unexpected src blend alpha %u for test %u.\n", d3d10_blend_desc.SrcBlendAlpha, i);
        ok(d3d10_blend_desc.DestBlendAlpha == current_desc->RenderTarget[0].DestBlendAlpha,
                "Got unexpected dest blend alpha %u for test %u.\n", d3d10_blend_desc.DestBlendAlpha, i);
        ok(d3d10_blend_desc.BlendOpAlpha == current_desc->RenderTarget[0].BlendOpAlpha,
                "Got unexpected blend op alpha %u for test %u.\n", d3d10_blend_desc.BlendOpAlpha, i);
        for (j = 0; j < D3D10_SIMULTANEOUS_RENDER_TARGET_COUNT; j++)
        {
            unsigned int k = current_desc->IndependentBlendEnable ? j : 0;
            ok(d3d10_blend_desc.BlendEnable[j] == current_desc->RenderTarget[k].BlendEnable,
                    "Got unexpected blend enable %#x for test %u, render target %u.\n",
                    d3d10_blend_desc.BlendEnable[j], i, j);
            ok(d3d10_blend_desc.RenderTargetWriteMask[j] == current_desc->RenderTarget[k].RenderTargetWriteMask,
                    "Got unexpected render target write mask %#x for test %u, render target %u.\n",
                    d3d10_blend_desc.RenderTargetWriteMask[j], i, j);
        }

        ID3D10BlendState_Release(d3d10_blend_state);

        refcount = ID3D10BlendState1_Release(blend_state1);
        ok(!refcount, "Got unexpected refcount %lu.\n", refcount);
    }

    refcount = ID3D10Device1_Release(device);
    ok(!refcount, "Device has %lu references left.\n", refcount);
}

static void test_getdc(void)
{
    struct device_desc device_desc;
    D3D10_TEXTURE2D_DESC desc;
    ID3D10Texture2D *texture;
    IDXGISurface1 *surface1;
    ID3D10Device1 *device;
    ULONG refcount;
    HRESULT hr;
    HDC dc;

    device_desc.feature_level = D3D10_FEATURE_LEVEL_10_1;
    device_desc.flags = D3D10_CREATE_DEVICE_BGRA_SUPPORT;
    if (!(device = create_device(&device_desc)))
    {
        skip("Failed to create device.\n");
        return;
    }

    /* Without D3D10_RESOURCE_MISC_GDI_COMPATIBLE. */
    desc.Width = 512;
    desc.Height = 512;
    desc.MipLevels = 1;
    desc.ArraySize = 1;
    desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
    desc.SampleDesc.Count = 1;
    desc.SampleDesc.Quality = 0;
    desc.Usage = D3D10_USAGE_DEFAULT;
    desc.BindFlags = D3D10_BIND_RENDER_TARGET;
    desc.CPUAccessFlags = 0;
    desc.MiscFlags = 0;
    hr = ID3D10Device1_CreateTexture2D(device, &desc, NULL, &texture);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);

    hr = ID3D10Texture2D_QueryInterface(texture, &IID_IDXGISurface1, (void**)&surface1);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);

    hr = IDXGISurface1_GetDC(surface1, FALSE, &dc);
    todo_wine ok(hr == DXGI_ERROR_INVALID_CALL, "Got unexpected hr %#lx.\n", hr);

    IDXGISurface1_Release(surface1);
    ID3D10Texture2D_Release(texture);

    desc.MiscFlags = D3D10_RESOURCE_MISC_GDI_COMPATIBLE;
    hr = ID3D10Device1_CreateTexture2D(device, &desc, NULL, &texture);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);

    hr = ID3D10Texture2D_QueryInterface(texture, &IID_IDXGISurface1, (void**)&surface1);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);

    hr = IDXGISurface1_ReleaseDC(surface1, NULL);
    todo_wine ok(hr == DXGI_ERROR_INVALID_CALL, "Got unexpected hr %#lx.\n", hr);

    hr = IDXGISurface1_GetDC(surface1, FALSE, &dc);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);

    /* One more time. */
    dc = (HDC)0xdeadbeef;
    hr = IDXGISurface1_GetDC(surface1, FALSE, &dc);
    todo_wine ok(hr == DXGI_ERROR_INVALID_CALL, "Got unexpected hr %#lx.\n", hr);
    ok(dc == (HDC)0xdeadbeef, "Got unexpected dc %p.\n", dc);

    hr = IDXGISurface1_ReleaseDC(surface1, NULL);
    ok(hr == S_OK, "Got unexpected hr %#lx.\n", hr);

    hr = IDXGISurface1_ReleaseDC(surface1, NULL);
    todo_wine ok(hr == DXGI_ERROR_INVALID_CALL, "Got unexpected hr %#lx.\n", hr);

    IDXGISurface1_Release(surface1);
    ID3D10Texture2D_Release(texture);

    refcount = ID3D10Device1_Release(device);
    ok(!refcount, "Device has %lu references left.\n", refcount);
}

struct resource_readback
{
    D3D10_RESOURCE_DIMENSION dimension;
    ID3D10Resource *resource;
    D3D10_MAPPED_TEXTURE2D map_desc;
    unsigned int width, height, sub_resource_idx;
};

static void get_texture_readback(ID3D10Texture2D *texture, unsigned int sub_resource_idx,
        struct resource_readback *rb)
{
    D3D10_TEXTURE2D_DESC texture_desc;
    unsigned int miplevel;
    ID3D10Device *device;
    HRESULT hr;

    memset(rb, 0, sizeof(*rb));
    rb->dimension = D3D10_RESOURCE_DIMENSION_TEXTURE2D;

    ID3D10Texture2D_GetDevice(texture, &device);

    ID3D10Texture2D_GetDesc(texture, &texture_desc);
    texture_desc.Usage = D3D10_USAGE_STAGING;
    texture_desc.BindFlags = 0;
    texture_desc.CPUAccessFlags = D3D10_CPU_ACCESS_READ;
    texture_desc.MiscFlags = 0;
    if (FAILED(hr = ID3D10Device_CreateTexture2D(device, &texture_desc, NULL, (ID3D10Texture2D **)&rb->resource)))
    {
        trace("Failed to create texture, hr %#x.\n", hr);
        ID3D10Device_Release(device);
        return;
    }

    miplevel = sub_resource_idx % texture_desc.MipLevels;
    rb->width = max(1, texture_desc.Width >> miplevel);
    rb->height = max(1, texture_desc.Height >> miplevel);
    rb->sub_resource_idx = sub_resource_idx;

    ID3D10Device_CopyResource(device, rb->resource, (ID3D10Resource *)texture);
    if (FAILED(hr = ID3D10Texture2D_Map((ID3D10Texture2D *)rb->resource, sub_resource_idx,
            D3D10_MAP_READ, 0, &rb->map_desc)))
    {
        trace("Failed to map sub-resource %u, hr %#x.\n", sub_resource_idx, hr);
        ID3D10Resource_Release(rb->resource);
        rb->resource = NULL;
    }

    ID3D10Device_Release(device);
}

struct vec3
{
    float x, y, z;
};

static void *get_readback_data(struct resource_readback *rb, unsigned int x, unsigned int y, unsigned byte_width)
{
    return (BYTE *)rb->map_desc.pData + y * rb->map_desc.RowPitch + x * byte_width;
}

static DWORD get_readback_u32(struct resource_readback *rb, unsigned int x, unsigned int y)
{
    return *(DWORD *)get_readback_data(rb, x, y, sizeof(DWORD));
}

static DWORD get_readback_color(struct resource_readback *rb, unsigned int x, unsigned int y)
{
    return get_readback_u32(rb, x, y);
}

static void release_resource_readback(struct resource_readback *rb)
{
    switch (rb->dimension)
    {
        case D3D10_RESOURCE_DIMENSION_BUFFER:
            ID3D10Buffer_Unmap((ID3D10Buffer *)rb->resource);
            break;
        case D3D10_RESOURCE_DIMENSION_TEXTURE1D:
            ID3D10Texture1D_Unmap((ID3D10Texture1D *)rb->resource, rb->sub_resource_idx);
            break;
        case D3D10_RESOURCE_DIMENSION_TEXTURE2D:
            ID3D10Texture2D_Unmap((ID3D10Texture2D *)rb->resource, rb->sub_resource_idx);
            break;
        default:
            trace("Unhandled resource dimension %#x.\n", rb->dimension);
            break;
    }
    ID3D10Resource_Release(rb->resource);
}

static BOOL compare_uint(unsigned int x, unsigned int y, unsigned int max_diff)
{
    unsigned int diff = x > y ? x - y : y - x;

    return diff <= max_diff;
}

static BOOL compare_color(DWORD c1, DWORD c2, BYTE max_diff)
{
    return compare_uint(c1 & 0xff, c2 & 0xff, max_diff)
            && compare_uint((c1 >> 8) & 0xff, (c2 >> 8) & 0xff, max_diff)
            && compare_uint((c1 >> 16) & 0xff, (c2 >> 16) & 0xff, max_diff)
            && compare_uint((c1 >> 24) & 0xff, (c2 >> 24) & 0xff, max_diff);
}

static void test_multisample_draw(D3D10_FEATURE_LEVEL1 feature_level)
{
    static const D3D10_INPUT_ELEMENT_DESC layout_desc[] =
    {
        {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0},
    };
    static const DWORD vs_code[] =
    {
#if 0
        float4 main(float4 position : POSITION) : SV_POSITION
        {
            return position;
        }
#endif
        0x43425844, 0x4fb19b86, 0x955fa240, 0x1a630688, 0x24eb9db4, 0x00000001, 0x000001e0, 0x00000006,
        0x00000038, 0x00000084, 0x000000d0, 0x00000134, 0x00000178, 0x000001ac, 0x53414e58, 0x00000044,
        0x00000044, 0xfffe0200, 0x00000020, 0x00000024, 0x00240000, 0x00240000, 0x00240000, 0x00240000,
        0x00240000, 0xfffe0200, 0x0200001f, 0x80000005, 0x900f0000, 0x02000001, 0xc00f0000, 0x80e40000,
        0x0000ffff, 0x50414e58, 0x00000044, 0x00000044, 0xfffe0200, 0x00000020, 0x00000024, 0x00240000,
        0x00240000, 0x00240000, 0x00240000, 0x00240000, 0xfffe0200, 0x0200001f, 0x80000005, 0x900f0000,
        0x02000001, 0xc00f0000, 0x80e40000, 0x0000ffff, 0x396e6f41, 0x0000005c, 0x0000005c, 0xfffe0200,
        0x00000034, 0x00000028, 0x00240000, 0x00240000, 0x00240000, 0x00240000, 0x00240001, 0x00000000,
        0xfffe0200, 0x0200001f, 0x80000005, 0x900f0000, 0x04000004, 0xc0030000, 0x90ff0000, 0xa0e40000,
        0x90e40000, 0x02000001, 0xc00c0000, 0x90e40000, 0x0000ffff, 0x52444853, 0x0000003c, 0x00010040,
        0x0000000f, 0x0300005f, 0x001010f2, 0x00000000, 0x04000067, 0x001020f2, 0x00000000, 0x00000001,
        0x05000036, 0x001020f2, 0x00000000, 0x00101e46, 0x00000000, 0x0100003e, 0x4e475349, 0x0000002c,
        0x00000001, 0x00000008, 0x00000020, 0x00000000, 0x00000000, 0x00000003, 0x00000000, 0x00000f0f,
        0x49534f50, 0x4e4f4954, 0xababab00, 0x4e47534f, 0x0000002c, 0x00000001, 0x00000008, 0x00000020,
        0x00000000, 0x00000001, 0x00000003, 0x00000000, 0x0000000f, 0x505f5653, 0x5449534f, 0x004e4f49,
    };
    static const DWORD ps_code[] =
    {
#if 0
        float4 main() : sv_target
        {
            return float4(1.0, 1.0, 1.0, 1.0);
        }
#endif
        0x43425844, 0x66f9215e, 0x5bf34f33, 0xdf28cbdf, 0x1d60aead, 0x00000001, 0x00000168, 0x00000005,
        0x00000034, 0x0000008c, 0x000000e4, 0x00000124, 0x00000134, 0x53414e58, 0x00000050, 0x00000050,
        0xffff0200, 0x0000002c, 0x00000024, 0x00240000, 0x00240000, 0x00240000, 0x00240000, 0x00240000,
        0xffff0200, 0x05000051, 0xa00f0000, 0x3f800000, 0x3f800000, 0x3f800000, 0x3f800000, 0x02000001,
        0x800f0800, 0xa0e40000, 0x0000ffff, 0x396e6f41, 0x00000050, 0x00000050, 0xffff0200, 0x0000002c,
        0x00000024, 0x00240000, 0x00240000, 0x00240000, 0x00240000, 0x00240000, 0xffff0200, 0x05000051,
        0xa00f0000, 0x3f800000, 0x3f800000, 0x3f800000, 0x3f800000, 0x02000001, 0x800f0800, 0xa0e40000,
        0x0000ffff, 0x52444853, 0x00000038, 0x00000040, 0x0000000e, 0x03000065, 0x001020f2, 0x00000000,
        0x08000036, 0x001020f2, 0x00000000, 0x00004002, 0x3f800000, 0x3f800000, 0x3f800000, 0x3f800000,
        0x0100003e, 0x4e475349, 0x00000008, 0x00000000, 0x00000008, 0x4e47534f, 0x0000002c, 0x00000001,
        0x00000008, 0x00000020, 0x00000000, 0x00000000, 0x00000003, 0x00000000, 0x0000000f, 0x745f7673,
        0x65677261, 0xabab0074,
    };
    static const struct vec3 tri[] =
    {
        { 0.9f, -0.5f, 0.0f},
        {-0.8f, -0.9f, 0.0f},
        { 0.2f,  0.7f, 0.0f},
    };
    static const float black[] = {0.0f, 0.0f, 0.0f, 1.0f};
    ID3D10Texture2D *texture, *resolve_texture;
    D3D10_TEXTURE2D_DESC texture_desc;
    ID3D10InputLayout *input_layout;
    D3D10_SUBRESOURCE_DATA vb_data;
    struct device_desc device_desc;
    D3D10_RASTERIZER_DESC rs_desc;
    struct resource_readback rb;
    ID3D10RenderTargetView *rtv;
    unsigned int stride, offset;
    D3D10_BUFFER_DESC vb_desc;
    ID3D10RasterizerState *rs;
    ID3D10VertexShader *vs;
    ID3D10PixelShader *ps;
    ID3D10Device1 *device;
    UINT quality_levels;
    D3D10_VIEWPORT vp;
    ID3D10Buffer *vb;
    unsigned int i;
    DWORD color;
    HRESULT hr;
    ULONG ref;

    static const struct
    {
        unsigned int x, y;
        DWORD colour_tri_ss, colour_tri_ms, colour_line_ss, colour_line_ms;
    }
    pixels[] =
    {
        {333, 407, 0xffffffff, 0xffffffff, 0xff000000, 0xff3f3f3f},
        {334, 407, 0xffffffff, 0xffffffff, 0xff000000, 0xff7f7f7f},
        {335, 407, 0xffffffff, 0xffffffff, 0xff000000, 0xffbfbfbf},
        {336, 407, 0xffffffff, 0xffffffff, 0xffffffff, 0xffbfbfbf},
        {337, 407, 0xffffffff, 0xffbfbfbf, 0xffffffff, 0xffffffff},
        {338, 407, 0xffffffff, 0xff7f7f7f, 0xffffffff, 0xffffffff},
        {339, 407, 0xff000000, 0xff3f3f3f, 0xffffffff, 0xffffffff},
        {340, 407, 0xff000000, 0xff3f3f3f, 0xffffffff, 0xffffffff},
        {341, 407, 0xff000000, 0xff000000, 0xffffffff, 0xffbfbfbf},
        {342, 407, 0xff000000, 0xff000000, 0xff000000, 0xffbfbfbf},
        {343, 407, 0xff000000, 0xff000000, 0xff000000, 0xff3f3f3f},
        {344, 407, 0xff000000, 0xff000000, 0xff000000, 0xff3f3f3f},
    };

    winetest_push_context("Feature level %x", feature_level);

    device_desc.feature_level = feature_level;
    device_desc.flags = 0;
    if (!(device = create_device(&device_desc)))
    {
        skip("Failed to create device.\n");
        winetest_pop_context();
        return;
    }

    hr = ID3D10Device1_CheckMultisampleQualityLevels(device, DXGI_FORMAT_R8G8B8A8_TYPELESS, 4, &quality_levels);
    ok(hr == S_OK, "Failed to check multisample quality levels, hr %#x.\n", hr);
    if (!quality_levels)
    {
        skip("4xMSAA not supported.\n");
        ID3D10Device1_Release(device);
        winetest_pop_context();
        return;
    }

    texture_desc.Width = 640;
    texture_desc.Height = 480;
    texture_desc.MipLevels = 1;
    texture_desc.ArraySize = 1;
    texture_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    texture_desc.SampleDesc.Count = 1;
    texture_desc.SampleDesc.Quality = 0;
    texture_desc.Usage = D3D10_USAGE_DEFAULT;
    texture_desc.BindFlags = D3D10_BIND_RENDER_TARGET;
    texture_desc.CPUAccessFlags = 0;
    texture_desc.MiscFlags = 0;
    hr = ID3D10Device1_CreateTexture2D(device, &texture_desc, NULL, &resolve_texture);
    texture_desc.SampleDesc.Count = 4;
    hr = ID3D10Device1_CreateTexture2D(device, &texture_desc, NULL, &texture);
    ok(hr == S_OK, "Failed to create texture, hr %#x.\n", hr);
    hr = ID3D10Device1_CreateRenderTargetView(device, (ID3D10Resource *)texture, NULL, &rtv);
    ok(hr == S_OK, "Failed to create render target view, hr %#x.\n", hr);
    vb_desc.ByteWidth = sizeof(tri);
    vb_desc.Usage = D3D10_USAGE_DEFAULT;
    vb_desc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
    vb_desc.CPUAccessFlags = 0;
    vb_desc.MiscFlags = 0;
    vb_data.pSysMem = tri;
    vb_data.SysMemPitch = vb_data.SysMemSlicePitch = 0;
    hr = ID3D10Device1_CreateBuffer(device, &vb_desc, &vb_data, &vb);
    ok(hr == S_OK, "Failed to create vertex buffer, hr %#x.\n", hr);

    hr = ID3D10Device1_CreateVertexShader(device, vs_code, sizeof(vs_code), &vs);
    ok(hr == S_OK, "Failed to create vertex shader, hr %#x.\n", hr);
    hr = ID3D10Device1_CreatePixelShader(device, ps_code, sizeof(ps_code), &ps);
    ok(hr == S_OK, "Failed to create pixel shader, hr %#x.\n", hr);
    hr = ID3D10Device1_CreateInputLayout(device, layout_desc, ARRAY_SIZE(layout_desc),
            vs_code, sizeof(vs_code), &input_layout);
    ok(hr == S_OK, "Failed to create input layout, hr %#x.\n", hr);

    rs_desc.FillMode = D3D10_FILL_SOLID;
    rs_desc.CullMode = D3D10_CULL_BACK;
    rs_desc.FrontCounterClockwise = FALSE;
    rs_desc.DepthBias = 0;
    rs_desc.DepthBiasClamp = 0.0f;
    rs_desc.SlopeScaledDepthBias = 0.0f;
    rs_desc.DepthClipEnable = TRUE;
    rs_desc.ScissorEnable = FALSE;
    rs_desc.MultisampleEnable = TRUE;
    rs_desc.AntialiasedLineEnable = FALSE;
    hr = ID3D10Device1_CreateRasterizerState(device, &rs_desc, &rs);
    ok(hr == S_OK, "Failed to create rasterizer state, hr %#x.\n", hr);

    stride = sizeof(*tri);
    offset = 0;
    ID3D10Device1_IASetVertexBuffers(device, 0, 1, &vb, &stride, &offset);
    ID3D10Device1_IASetInputLayout(device, input_layout);
    ID3D10Device1_IASetPrimitiveTopology(device, D3D10_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
    ID3D10Device1_VSSetShader(device, vs);
    vp.TopLeftX = vp.TopLeftY = 0;
    vp.Width = texture_desc.Width;
    vp.Height = texture_desc.Height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    ID3D10Device1_RSSetViewports(device, 1, &vp);
    ID3D10Device1_PSSetShader(device, ps);
    ID3D10Device1_OMSetRenderTargets(device, 1, &rtv, NULL);

    ID3D10Device1_ClearRenderTargetView(device, rtv, black);
    ID3D10Device1_Draw(device, 4, 0);
    ID3D10Device1_ResolveSubresource(device, (ID3D10Resource *)resolve_texture, 0,
            (ID3D10Resource *)texture, 0, texture_desc.Format);

    get_texture_readback(resolve_texture, 0, &rb);
    for (i = 0; i < ARRAY_SIZE(pixels); ++i)
    {
        color = get_readback_color(&rb, pixels[i].x, pixels[i].y);
        ok(compare_color(color, pixels[i].colour_tri_ms, 1)
                || broken(compare_color(color, pixels[i].colour_tri_ss, 1)),
                "Got unexpected color 0x%08x at (%u, %u).\n", color, pixels[i].x, pixels[i].y);
    }
    release_resource_readback(&rb);

    ID3D10Device1_RSSetState(device, rs);

    ID3D10Device1_ClearRenderTargetView(device, rtv, black);
    ID3D10Device1_Draw(device, 4, 0);
    ID3D10Device1_ResolveSubresource(device, (ID3D10Resource *)resolve_texture, 0,
            (ID3D10Resource *)texture, 0, texture_desc.Format);

    get_texture_readback(resolve_texture, 0, &rb);
    for (i = 0; i < ARRAY_SIZE(pixels); ++i)
    {
        color = get_readback_color(&rb, pixels[i].x, pixels[i].y);
        ok(compare_color(color, pixels[i].colour_tri_ms, 1),
                "Got unexpected color 0x%08x at (%u, %u).\n", color, pixels[i].x, pixels[i].y);
    }
    release_resource_readback(&rb);

    ID3D10Device1_IASetPrimitiveTopology(device, D3D10_PRIMITIVE_TOPOLOGY_LINELIST);
    ID3D10Device1_RSSetState(device, NULL);

    ID3D10Device1_ClearRenderTargetView(device, rtv, black);
    ID3D10Device1_Draw(device, 4, 0);
    ID3D10Device1_ResolveSubresource(device, (ID3D10Resource *)resolve_texture, 0,
            (ID3D10Resource *)texture, 0, texture_desc.Format);

    get_texture_readback(resolve_texture, 0, &rb);
    for (i = 0; i < ARRAY_SIZE(pixels); ++i)
    {
        color = get_readback_color(&rb, pixels[i].x, pixels[i].y);
        todo_wine_if (pixels[i].colour_line_ms != 0xff000000 && pixels[i].colour_line_ms != 0xffffffff)
            ok(compare_color(color, pixels[i].colour_line_ss, 1),
                    "Got unexpected color 0x%08x at (%u, %u).\n", color, pixels[i].x, pixels[i].y);
    }
    release_resource_readback(&rb);

    ID3D10Device1_RSSetState(device, rs);

    ID3D10Device1_ClearRenderTargetView(device, rtv, black);
    ID3D10Device1_Draw(device, 4, 0);
    ID3D10Device1_ResolveSubresource(device, (ID3D10Resource *)resolve_texture, 0,
            (ID3D10Resource *)texture, 0, texture_desc.Format);

    get_texture_readback(resolve_texture, 0, &rb);
    for (i = 0; i < ARRAY_SIZE(pixels); ++i)
    {
        color = get_readback_color(&rb, pixels[i].x, pixels[i].y);
        /* D3D uses a wider quad (1.4 units) than GL or Vulkan. It may not be possible to
         * make this work exactly like D3D.
         */
        todo_wine_if (i != 3 && i != 5 && i != 10)
            ok(compare_color(color, pixels[i].colour_line_ms, 1),
                    "Got unexpected color 0x%08x at (%u, %u).\n", color, pixels[i].x, pixels[i].y);
    }
    release_resource_readback(&rb);

    ID3D10InputLayout_Release(input_layout);
    ID3D10VertexShader_Release(vs);
    ID3D10PixelShader_Release(ps);
    ID3D10RasterizerState_Release(rs);
    ID3D10RenderTargetView_Release(rtv);
    ID3D10Texture2D_Release(texture);
    ID3D10Texture2D_Release(resolve_texture);
    ID3D10Buffer_Release(vb);
    ref = ID3D10Device1_Release(device);
    ok(!ref, "Device has %u references left.\n", ref);

    winetest_pop_context();
}

START_TEST(d3d10_1)
{
    unsigned int i;

    test_create_device();
    test_device_interfaces();
    test_create_shader_resource_view();
    test_create_blend_state();
    test_getdc();

    for (i = 0; i < ARRAY_SIZE(d3d10_feature_levels); i++)
        test_multisample_draw(d3d10_feature_levels[i]);
}
