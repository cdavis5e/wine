/*
 * Power management support
 *
 * Copyright (C) 1998 Alexandre Julliard
 * Copyright (C) 2003 Mike McCormack
 * Copyright (C) 2005 Robert Shearman
 * Copyright (C) 2019 Chip Davis for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"

#include <assert.h>
#include <stdio.h>
#include <stdarg.h>

#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "windef.h"
#include "winternl.h"

#include "handle.h"
#include "request.h"
#include "thread.h"
#include "unicode.h"

static unsigned int sys_count = 0;  /* number of holds on system sleep */
static unsigned int disp_count = 0; /* number of holds on display sleep */
static unsigned int away_count = 0; /* number of away mode requests */

void release_thread_execution_state( struct thread *thread )
{
    if (thread->exec_state & ES_SYSTEM_REQUIRED)
        --sys_count;
    if (thread->exec_state & ES_DISPLAY_REQUIRED)
        --disp_count;
    if (thread->exec_state & ES_AWAYMODE_REQUIRED)
        --away_count;
}


/* Get the current system execution state */
DECL_HANDLER(get_system_execution_state)
{
    reply->exec_state = 0;
    if (sys_count != 0)
        reply->exec_state |= ES_SYSTEM_REQUIRED;
    if (disp_count != 0)
        reply->exec_state |= ES_DISPLAY_REQUIRED;
    if (away_count != 0)
        reply->exec_state |= ES_AWAYMODE_REQUIRED;
}

/* Set the current thread's execution state */
DECL_HANDLER(set_thread_execution_state)
{
    reply->old_state = current->exec_state;

    if (!(req->new_state & ES_CONTINUOUS))
        return;

    if ((current->exec_state & ES_SYSTEM_REQUIRED) && !(req->new_state & ES_SYSTEM_REQUIRED))
        --sys_count;
    else if (!(current->exec_state & ES_SYSTEM_REQUIRED) && (req->new_state & ES_SYSTEM_REQUIRED))
        ++sys_count;

    if ((current->exec_state & ES_DISPLAY_REQUIRED) && !(req->new_state & ES_DISPLAY_REQUIRED))
        --disp_count;
    else if (!(current->exec_state & ES_DISPLAY_REQUIRED) && (req->new_state & ES_DISPLAY_REQUIRED))
        ++disp_count;

    if ((current->exec_state & ES_AWAYMODE_REQUIRED) && !(req->new_state & ES_AWAYMODE_REQUIRED))
        --away_count;
    else if (!(current->exec_state & ES_AWAYMODE_REQUIRED) && (req->new_state & ES_AWAYMODE_REQUIRED))
        ++away_count;
    current->exec_state = req->new_state;
}
